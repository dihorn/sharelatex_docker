# Nimm die alte Version
FROM sharelatex/sharelatex:2.2.0

# Update texlive/tlmgr nach dieser Anleitung:
# https://www.tug.org/texlive/upgrade.html
WORKDIR /usr/local/texlive
# RUN cp -a 2019 2020
ENV PATH="/usr/local/texlive/2019/bin/x86_64-linux:$PATH"
RUN curl -OL http://mirror.ctan.org/systems/texlive/tlnet/update-tlmgr-latest.sh
RUN sh update-tlmgr-latest.sh -- --upgrade

# Update alle vorhandenen Pakete und installiere
# die Pakete, auf die scheme-medium dependet.
#RUN tlmgr update --self --all
RUN tlmgr install scheme-full
